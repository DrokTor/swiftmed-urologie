<?php

namespace app\controllers;

use Yii;
use app\models\Visites;
use app\models\UroInterrogatoire;
use app\models\UroExploration;
use app\models\UroBiologique;
use app\models\UroRadiologique;
use app\models\UroDiagnostique;
use app\models\UroAnapath;
use app\models\UroTraitement;


use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VisitesController implements the CRUD actions for Visites model.
 */
class VisitesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Visites models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Visites::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Visites model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Visites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Visites();
        $model = new UroInterrogatoire();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Visites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Visites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Visites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Visites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */


    public function actionVisit()
    {
      if(Yii::$app->request->isAjax)
      {
      $post=Yii::$app->request->Post();
      $visite=Visites::find()->with('interro','explo','bio','radio','diagno','anapat','traitement')->where(['visites.id'=>$post['id']])->asArray()->one();
      //dd($visite);

       \Yii::$app->response->format = 'json';
       //dd($visite);
      return $visite;
      }

    }
    public function actionSaveVisit()
    {
      if(Yii::$app->request->isAjax)
      {
      $post=Yii::$app->request->Post();
      $visite= new Visites();
      $interrogatoire= new UroInterrogatoire();
      $exploration= new UroExploration();
      $biologique= new UroBiologique();
      $radiologique= new UroRadiologique();
      $diagnostic= new UroDiagnostique();
      $anapath= new UroAnapath();
      $traitement= new UroTraitement();

      $load=[];
      /*
      dd($post);
      foreach ($post['form'] as $key => $value) {
         $load[$value['name']]=$value['value'];
      }
      //dd($load);
      //$post['date_visite']=$post['date'];
      $load=['Visites'=>$load];*/
      $interrogatoire->load($post);
      $interrogatoire->save();
      $exploration->load($post);
      $exploration->save();
      $biologique->load($post);
      $biologique->save();
      $radiologique->load($post);
      $radiologique->save();
      $diagnostic->load($post);
      $diagnostic->save();
      $anapath->load($post);
      $anapath->save();
      $traitement->load($post);
      $traitement->save();

      $visite->patient_id= $post['id'];;
      //$visite->diagnostic=$post['diag'];
      //$visite->prescription=$post['pres'];
      $visite->date_visite=$this->frdatetosql($post['date']);
      //dd($this->frdatetosql($post['date']));
      $visite->interogatoire=$interrogatoire->id;
      $visite->exploration_fonctionnelle=$exploration->id;
      $visite->examen_biologique=$biologique->id;
      $visite->examen_radiologique=$radiologique->id;
      $visite->diagnostic=$diagnostic->id;
      $visite->anapath=$anapath->id;
      $visite->prescription=$traitement->id;

      $visite->load($post);
      if($visite->save())
      {

      }else dd($visite->errors);



      return true;
      }

    }
    protected function findModel($id)
    {
        if (($model = Visites::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function frdatetosql($date)
    {
      //convert frenhc date to date
     $find = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
     $replace = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
     $date = str_replace($find, $replace, strtolower($date));
     $date = date('Y-m-d', strtotime($date));

     return $date;
    }
}
