<?php

use yii\db\Migration;

/**
 * Handles the creation of table `patients`.
 */
class m170613_142439_create_patients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('patients', [
            'id' => $this->primaryKey(),
            'nom' => $this->string(),
            'prenom' => $this->string(),
            'naissance' => $this->date(),
            'genre' => $this->integer(),
            'adresse' => $this->string(),
            'ville' => $this->integer(),
            'tel' => $this->string(),
            'email' => $this->string(),
            'profession' => $this->string(),
            'antecedents_per' => $this->string(),
            'antecedents_fam' => $this->string(),
            'description' => $this->string(),
            'next_visit' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('patients');
    }
}
