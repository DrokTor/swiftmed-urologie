<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uro_interrogatoire`.
 */
class m170730_125711_create_uro_interrogatoire_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uro_interrogatoire', [
            'id' => $this->primaryKey(),
            'hemodure' => $this->string(),
            'douleur' => $this->string(),
            'masse' => $this->string(),
            'grosse' => $this->string(),
            'psa' => $this->string(),
            'tuba' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uro_interrogatoire');
    }
}
