<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uro_exploration`.
 */
class m170802_160115_create_uro_exploration_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uro_exploration', [
            'id' => $this->primaryKey(),
            'debimetrie' => $this->string(),
            'cystomano' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uro_exploration');
    }
}
