<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uro_biologique`.
 */
class m170803_135838_create_uro_biologique_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uro_biologique', [
            'id' => $this->primaryKey(),
            'uree' => $this->string(),
            'creat' => $this->string(),
            'mdrd' => $this->string(),
            'psa' => $this->string(),
            'bhcg' => $this->string(),
            'afp' => $this->string(),
            'ldh' => $this->string(),
            'cytologie' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uro_biologique');
    }
}
