<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uro_radiologique`.
 */
class m170803_153449_create_uro_radiologique_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uro_radiologique', [
            'id' => $this->primaryKey(),
            'asp' => $this->string(),
            'echo' => $this->string(),
            'uroscanner' => $this->string(),
            'uroirm' => $this->string(),
            'densite' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uro_radiologique');
    }
}
