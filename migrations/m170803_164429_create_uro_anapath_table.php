<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uro_anapath`.
 */
class m170803_164429_create_uro_anapath_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uro_anapath', [
            'id' => $this->primaryKey(),
            'tvnimbg' => $this->string(),
            'tvnimhg' => $this->string(),
            'tvim' => $this->string(),
            'nephrectomie' => $this->string(),
            'orchidectomie' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uro_anapath');
    }
}
