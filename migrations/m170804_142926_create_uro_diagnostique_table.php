<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uro_diagnostique`.
 */
class m170804_142926_create_uro_diagnostique_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uro_diagnostique', [
            'id' => $this->primaryKey(),
            'tvs' => $this->string(),
            'tvi' => $this->string(),
            'cr_prostate' => $this->string(),
            'cr_rein' => $this->string(),
            'cr_testicule' => $this->string(),
            'li_renale' => $this->string(),
            'li_ureterale' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uro_diagnostique');
    }
}
