<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uro_traitement`.
 */
class m170804_163202_create_uro_traitement_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uro_traitement', [
            'id' => $this->primaryKey(),
            'mmc' => $this->string(),
            'bcg' => $this->string(),
            'prostatectomie' => $this->string(),
            'ana_lh_rh' => $this->string(),
            'chimio' => $this->string(),
            'antiandro' => $this->string(),
            'cystopro' => $this->string(),
            'bricker' => $this->string(),
            'remp_vessie' => $this->string(),
            'nephrec' => $this->string(),
            'orchidec' => $this->string(),
            'urs_rigide' => $this->string(),
            'urs_souple' => $this->string(),
            'lithotripsie' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uro_traitement');
    }
}
