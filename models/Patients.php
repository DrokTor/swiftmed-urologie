<?php

namespace app\models;

use Yii;
use app\models\Wilayas;
use app\models\Genres;

/**
 * This is the model class for table "patients".
 *
 * @property integer $id
 * @property string $nom
 * @property string $prenom
 * @property string $naissance
 * @property integer $genre
 * @property string $adresse
 * @property integer $ville
 * @property string $tel
 * @property string $description
 */
class Patients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['naissance','next_visit'], 'safe'],
            [['genre', 'ville'], 'integer'],
            [['nom', 'prenom','naissance','genre', 'ville'], 'required'],
            [['email'], 'email'],
            [['nom', 'prenom', 'adresse', 'tel','profession','antecedents_per','antecedents_fam', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nom' => 'Nom',
            'prenom' => 'Prenom',
            'naissance' => 'Date de naissance',
            'genre' => 'Genre',
            'adresse' => 'Adresse',
            'ville' => 'Ville',
            'tel' => 'Tel',
            'email' => 'Email',
            'profession' => 'Profession',
            'antecedents_per' => 'Antécédents personnels',
            'antecedents_fam' => 'Antécédents familiaux',
            'description' => 'Note',
        ];
    }

    public function getWilaya()
    {
      return $this->hasOne(Wilayas::className(),['code'=>'ville']);
    }
    public function getSex()
    {
      return $this->hasOne(Genres::className(),['id'=>'genre']);
    }
}
