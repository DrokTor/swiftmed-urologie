<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uro_anapath".
 *
 * @property integer $id
 * @property string $tvnimbg
 * @property string $tvnimhg
 * @property string $tvim
 * @property string $nephrectomie
 * @property string $orchidectomie
 */
class UroAnapath extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uro_anapath';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tvnimbg', 'tvnimhg', 'tvim', 'nephrectomie', 'orchidectomie'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tvnimbg' => 'Tvnimbg',
            'tvnimhg' => 'Tvnimhg',
            'tvim' => 'Tvim',
            'nephrectomie' => 'Nephrectomie',
            'orchidectomie' => 'Orchidectomie',
        ];
    }
}
