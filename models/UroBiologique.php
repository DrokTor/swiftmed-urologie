<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uro_biologique".
 *
 * @property integer $id
 * @property string $uree
 * @property string $creat
 * @property string $mdrd
 * @property string $psa
 * @property string $bhcg
 * @property string $afp
 * @property string $ldh
 * @property string $cytologie
 */
class UroBiologique extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uro_biologique';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uree', 'creat', 'mdrd', 'psa', 'bhcg', 'afp', 'ldh', 'cytologie'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uree' => 'Uree',
            'creat' => 'Creat',
            'mdrd' => 'Mdrd',
            'psa' => 'Psa',
            'bhcg' => 'Bhcg',
            'afp' => 'Afp',
            'ldh' => 'Ldh',
            'cytologie' => 'Cytologie',
        ];
    }
}
