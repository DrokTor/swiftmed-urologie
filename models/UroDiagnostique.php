<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uro_diagnostique".
 *
 * @property integer $id
 * @property string $tvs
 * @property string $tvi
 * @property string $cr_prostate
 * @property string $cr_rein
 * @property string $cr_testicule
 * @property string $li_renale
 * @property string $li_ureterale
 */
class UroDiagnostique extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uro_diagnostique';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tvs', 'tvi', 'cr_prostate', 'cr_rein', 'cr_testicule', 'li_renale', 'li_ureterale'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tvs' => 'Tvs',
            'tvi' => 'Tvi',
            'cr_prostate' => 'Cr Prostate',
            'cr_rein' => 'Cr Rein',
            'cr_testicule' => 'Cr Testicule',
            'li_renale' => 'Li Renale',
            'li_ureterale' => 'Li Ureterale',
        ];
    }
}
