<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uro_exploration".
 *
 * @property integer $id
 * @property string $devimetrie
 * @property string $cystomano
 */
class UroExploration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uro_exploration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['debimetrie', 'cystomano'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'debimetrie' => 'Debimetrie',
            'cystomano' => 'Cystomano',
        ];
    }
}
