<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uro_interrogatoire".
 *
 * @property integer $id
 * @property string $hemodure
 * @property string $douleur
 * @property string $masse
 * @property string $grosse
 * @property string $psa
 * @property string $tuba
 */
class UroInterrogatoire extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uro_interrogatoire';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hemodure', 'douleur', 'masse', 'grosse', 'psa', 'tuba'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hemodure' => 'Hemodure',
            'douleur' => 'Douleur Lombaire',
            'masse' => 'Masse Rénale',
            'grosse' => 'Grosse Bourse',
            'psa' => 'Elevation du Psa',
            'tuba' => 'Tuba',
        ];
    }
}
