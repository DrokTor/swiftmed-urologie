<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uro_radiologique".
 *
 * @property integer $id
 * @property string $asp
 * @property string $echo
 * @property string $uroscanner
 * @property string $uroirm
 * @property string $densite
 */
class UroRadiologique extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uro_radiologique';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asp', 'echo', 'uroscanner', 'uroirm', 'densite'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'asp' => 'Asp',
            'echo' => 'Echo',
            'uroscanner' => 'Uroscanner',
            'uroirm' => 'Uroirm',
            'densite' => 'Densite',
        ];
    }
}
