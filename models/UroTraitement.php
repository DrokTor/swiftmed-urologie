<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uro_traitement".
 *
 * @property integer $id
 * @property string $mmc
 * @property string $bcg
 * @property string $prostatectomie
 * @property string $ana_lh_rh
 * @property string $chimio
 * @property string $antiandro
 * @property string $cystopro
 * @property string $bricker
 * @property string $remp_vessie
 * @property string $nephrec
 * @property string $orchidec
 * @property string $urs_rigide
 * @property string $urs_souple
 * @property string $lithotripsie
 */
class UroTraitement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uro_traitement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mmc', 'bcg', 'prostatectomie', 'ana_lh_rh', 'chimio', 'antiandro', 'cystopro', 'bricker', 'remp_vessie', 'nephrec', 'orchidec', 'urs_rigide', 'urs_souple', 'lithotripsie'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mmc' => 'Mmc',
            'bcg' => 'Bcg',
            'prostatectomie' => 'Prostatectomie',
            'ana_lh_rh' => 'Ana Lh Rh',
            'chimio' => 'Chimio',
            'antiandro' => 'Antiandro',
            'cystopro' => 'Cystopro',
            'bricker' => 'Bricker',
            'remp_vessie' => 'Remp Vessie',
            'nephrec' => 'Nephrec',
            'orchidec' => 'Orchidec',
            'urs_rigide' => 'Urs Rigide',
            'urs_souple' => 'Urs Souple',
            'lithotripsie' => 'Lithotripsie',
        ];
    }
}
