<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visites".
 *
 * @property integer $id
 * @property integer $patient_id
 * @property string $date_visite
 * @property string $diagnostic
 * @property string $prescription
 *
 * @property Patients $patient
 */
class Visites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id','interogatoire','diagnostic','anapath','exploration_fonctionnelle','examen_biologique','examen_radiologique','prescription'], 'integer'],
            [['date_visite'], 'safe'],
            [[ 'motif','examen_clinique','prevention'], 'string', 'max' => 255],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patients::className(), 'targetAttribute' => ['patient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'date_visite' => 'Date Visite',
            'diagnostic' => 'Diagnostic',
            'prescription' => 'Prescription',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patients::className(), ['id' => 'patient_id']);
    }
    public function getInterro()
    {
        return $this->hasOne(UroInterrogatoire::className(), ['id' => 'interogatoire']);
    }
    public function getExplo()
    {
        return $this->hasOne(UroExploration::className(), ['id' => 'exploration_fonctionnelle']);
    }
    public function getBio()
    {
        return $this->hasOne(UroBiologique::className(), ['id' => 'examen_biologique']);
    }
    public function getRadio()
    {
        return $this->hasOne(UroRadiologique::className(), ['id' => 'examen_radiologique']);
    }
    public function getDiagno()
    {
        return $this->hasOne(UroDiagnostique::className(), ['id' => 'diagnostic']);
    }
    public function getAnapat()
    {
        return $this->hasOne(UroAnapath::className(), ['id' => 'anapath']);
    }
    public function getTraitement()
    {
        return $this->hasOne(UroTraitement::className(), ['id' => 'prescription']);
    }
}
