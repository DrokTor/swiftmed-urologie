<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Pjax;

AppAsset::register($this);
\conquer\momentjs\MomentjsAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => '<img src="images/logo.png" class="img-responsive col-md-4"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Liste des Patients','linkOptions'=>['class'=>'ajax-request'], 'url' => ['/site/index-ajax']],
            ['label' => 'Nouveau Patient','linkOptions'=>['class'=>'ajax-request'], 'url' => ['/site/create-ajax']],
            //['label' => 'Visites', 'url' => ['/site/about']],


        ],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?php /* Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */?>
        <?= $content ?>
    </div>
    <div id="loading" class="collapse"><div><img src="images/loading.svg"/></div></div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><a href="http://www.optidev-dz.com">&copy; optidev</a> <?= date('Y') ?></p>

     </div>
</footer>
<?php $this->endBody() ?>
</body>

<script>
  $(document).ready(function() {
$(document).on('click','#historique .visite', function(event) {
  //console.log($(event.target).data('value'));
  $('.visite_heading').text('Date de la visite:');
  $('.selected').toggleClass('selected');
  $(event.target).toggleClass('selected');
  val= $(event.target).data('value');

  if($('#new-visit-container').length)
  $('#new-visit-container').fadeOut(500,function(){
    $('#visit-container').fadeIn(500);

  });
  //else $('#new-visit-container').fadeIn(500);

  moment.locale('fr');

  $.post('/visites/visit', {id: val}, function(data, textStatus, xhr) {
    date=moment(data.date_visite).format("Do MMM YYYY");
    //console.log(data);
    $('#date-visite').fadeOut(500, function() {
        $(this).text(date).fadeIn(500);


    });

    $('#print-link').fadeOut(500, function() {
        $(this).attr('href',"ordonnance?id="+val).fadeIn(500);
    });

    $('#motif_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.motif).fadeIn(500);
    });
    $('#hemodure_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.interro.hemodure).fadeIn(500);
    });
    $('#douleur_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.interro.douleur).fadeIn(500);
    });
    $('#masse_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.interro.masse).fadeIn(500);
    });
    $('#grosse_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.interro.grosse).fadeIn(500);
    });
    $('#psa_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.interro.psa).fadeIn(500);
    });
    $('#tuba_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.interro.tuba).fadeIn(500);
    });
    $('#exam_cli_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.examen_clinique).fadeIn(500);
    });/*
    $('#exam_bio_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.examen_biologique).fadeIn(500);
    });*/
    $('#uree_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.uree).fadeIn(500);
    });
    $('#creat_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.creat).fadeIn(500);
    });
    $('#mdrd_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.mdrd).fadeIn(500);
    });
    $('#biopsa_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.psa).fadeIn(500);
    });
    $('#bhcg_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.bhcg).fadeIn(500);
    });
    $('#afp_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.afp).fadeIn(500);
    });
    $('#ldh_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.ldh).fadeIn(500);
    });
    $('#cytologie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.bio.cytologie).fadeIn(500);
    });
    //radiologique
    $('#asp_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.radio.asp).fadeIn(500);
    });
    $('#echo_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.radio.echo).fadeIn(500);
    });
    $('#uroscanner_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.radio.uroscanner).fadeIn(500);
    });
    $('#uroirm_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.radio.uroirm).fadeIn(500);
    });
    $('#densite_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.radio.densite).fadeIn(500);
    });
/*
    $('#exam_radio_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.examen_radiologique).fadeIn(500);
    });
    $('#exploration_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.exploration_fonctionnelle).fadeIn(500);
    });*/
    $('#debimetrie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.explo.debimetrie).fadeIn(500);
    });
    $('#cystomano_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.explo.cystomano).fadeIn(500);
    });

    $('#prevention_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.prevention).fadeIn(500);
    });

    $('#diagnostic_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagnostic).fadeIn(500);
    });
    $('#prescription_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.prescription).fadeIn(500);
    });

    //Diagnostic
    $('#tvs_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagno.tvs).fadeIn(500);
    });
    $('#tvi_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagno.tvi).fadeIn(500);
    });
    $('#cr_prostate_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagno.cr_prostate).fadeIn(500);
    });
    $('#cr_rein_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagno.cr_rein).fadeIn(500);
    });
    $('#cr_testicule_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagno.cr_testicule).fadeIn(500);
    });
    $('#li_renale_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagno.li_renale).fadeIn(500);
    });
    $('#li_ureterale_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagno.li_ureterale).fadeIn(500);
    });

    //anapath
    $('#tvnimbg_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.anapat.tvnimbg).fadeIn(500);
    });
    $('#tvnimhg_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.anapat.tvnimhg).fadeIn(500);
    });
    $('#tvim_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.anapat.tvim).fadeIn(500);
    });
    $('#nephrectomie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.anapat.nephrectomie).fadeIn(500);
    });
    $('#orchidectomie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.anapat.orchidectomie).fadeIn(500);
    });

    //traitement
    $('#mmc_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.mmc).fadeIn(500);
    });
    $('#bcg_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.bcg).fadeIn(500);
    });
    $('#prostatectomie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.prostatectomie).fadeIn(500);
    });
    $('#nephrectomie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.nephrectomie).fadeIn(500);
    });
    $('#ana_lh_rh_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.ana_lh_rh).fadeIn(500);
    });
    $('#chimio_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.chimio).fadeIn(500);
    });
    $('#antiandro_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.antiandro).fadeIn(500);
    });
    $('#cystopro_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.cystopro).fadeIn(500);
    });
    $('#bricker_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.bricker).fadeIn(500);
    });
    $('#remp_vessie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.remp_vessie).fadeIn(500);
    });
    $('#nephrec_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.nephrec).fadeIn(500);
    });
    $('#orchidec_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.orchidec).fadeIn(500);
    });
    $('#urs_rigide_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.urs_rigide).fadeIn(500);
    });
    $('#urs_souple_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.urs_souple).fadeIn(500);
    });
    $('#lithotripsie_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.traitement.lithotripsie).fadeIn(500);
    });

  });
});

$(document).on('click','#new-visit', function(event) {

  moment.locale('fr');
  date=moment().format("Do MMMM YYYY");

  if($('#visit-container').length)
  $('#visit-container').fadeOut(500,function(){
    $('#new-visit-container').fadeIn(500);

  });
  else $('#new-visit-container').fadeIn(500);



  $('#date-visite').fadeOut(500, function() {
      $(this).text(date).fadeIn(500);
  });
  // $('#diagnostic').fadeOut(500, function() {
  //     $(this).html('<textarea class="col-md-12"></textarea>').fadeIn(500);
  // });
  // $('#prescription').fadeOut(500, function() {
  //     $(this).html('<textarea class="col-md-12"></textarea>').fadeIn(500);
  // });
  $('#save-visit').fadeIn(500);
});


$(document).on('click','#save-visit', function(event) {
  event.preventDefault();

   answer=confirm("êtes-vous sûr de vouloir clôturer la visite ?");


  if(!answer) return false;
  id=$('#id-patient').text();
  date=$('#date-visite').text();
  //diag=$('#diagnostic').children('textarea')[0].value;
  //pres=$('#prescription').children('textarea')[0].value;
  form= $('#new-visit-container').serialize()
  formArray= $('#new-visit-container').serializeArray()
  //console.log(formArray);
  //return false;
  form='id='+id+'&'+'date='+date+'&'+form;
  $.post('/visites/save-visit', form/*{id:id, date: date, form/*:form  }*/, function(data, textStatus, xhr) {

    $('#new-visit-container').fadeOut(500,function(){
      $('#visit-container').fadeIn(500);
      $('#new-visit-container textarea').each(function(index, el) {
        el.value='';
        //console.log(el.value);
      });
    });

    $('#date-visite').fadeOut(500, function() {
        $(this).text(date).fadeIn(500);
    });

    $('#motif_record').fadeOut(500, function() {
        $(this).text(formArray[0].value).fadeIn(500);
    });
    // Interrogatoire
    $('#hemodure_record').fadeOut(500, function() {
        $(this).text(formArray[1].value).fadeIn(500);
    });
    $('#douleur_record').fadeOut(500, function() {
        $(this).text(formArray[2].value).fadeIn(500);
    });
    $('#masse_record').fadeOut(500, function() {
        $(this).text(formArray[3].value).fadeIn(500);
    });
    $('#grosse_record').fadeOut(500, function() {
        $(this).text(formArray[4].value).fadeIn(500);
    });
    $('#psa_record').fadeOut(500, function() {
        $(this).text(formArray[5].value).fadeIn(500);
    });
    $('#tuba_record').fadeOut(500, function() {
        $(this).text(formArray[6].value).fadeIn(500);
    });
    // examen clinique
    $('#exam_cli_record').fadeOut(500, function() {
        $(this).text(formArray[7].value).fadeIn(500);
    });

    //examen biologique
    $('#uree_record').fadeOut(500, function() {
        $(this).text(formArray[8].value).fadeIn(500);
    });
    $('#creat_record').fadeOut(500, function() {
        $(this).text(formArray[9].value).fadeIn(500);
    });
    $('#mdrd_record').fadeOut(500, function() {
        $(this).text(formArray[10].value).fadeIn(500);
    });
    $('#biopsa_record').fadeOut(500, function() {
        $(this).text(formArray[11].value).fadeIn(500);
    });
    $('#bhcg_record').fadeOut(500, function() {
        $(this).text(formArray[12].value).fadeIn(500);
    });
    $('#afp_record').fadeOut(500, function() {
        $(this).text(formArray[13].value).fadeIn(500);
    });
    $('#ldh_record').fadeOut(500, function() {
        $(this).text(formArray[14].value).fadeIn(500);
    });
    $('#cytologie_record').fadeOut(500, function() {
        $(this).text(formArray[15].value).fadeIn(500);
    });
    // examen radiologique

    $('#asp_record').fadeOut(500, function() {
        $(this).text(formArray[16].value).fadeIn(500);
    });
    $('#echo_record').fadeOut(500, function() {
        $(this).text(formArray[17].value).fadeIn(500);
    });
    $('#uroscanner_record').fadeOut(500, function() {
        $(this).text(formArray[18].value).fadeIn(500);
    });
    $('#uroirm_record').fadeOut(500, function() {
        $(this).text(formArray[19].value).fadeIn(500);
    });
    $('#densite_record').fadeOut(500, function() {
        $(this).text(formArray[20].value).fadeIn(500);
    });


    // examen fonctionnel
    $('#debimetrie_record').fadeOut(500, function() {
        $(this).text(formArray[21].value).fadeIn(500);
    });
    $('#cystomano_record').fadeOut(500, function() {
        $(this).text(formArray[22].value).fadeIn(500);
    });


    // Diagnostic

    $('#tvs_record').fadeOut(500, function() {
        $(this).text(formArray[23].value).fadeIn(500);
    });
    $('#tvi_record').fadeOut(500, function() {
        $(this).text(formArray[24].value).fadeIn(500);
    });
    $('#cr_prostate_record').fadeOut(500, function() {
        $(this).text(formArray[25].value).fadeIn(500);
    });
    $('#cr_rein_record').fadeOut(500, function() {
        $(this).text(formArray[26].value).fadeIn(500);
    });
    $('#cr_testicule_record').fadeOut(500, function() {
        $(this).text(formArray[27].value).fadeIn(500);
    });
    $('#li_renale_record').fadeOut(500, function() {
        $(this).text(formArray[28].value).fadeIn(500);
    });
    $('#li_ureterale_record').fadeOut(500, function() {
        $(this).text(formArray[29].value).fadeIn(500);
    });

    // Anapath

    $('#tvnimbg_record').fadeOut(500, function() {
        $(this).text(formArray[30].value).fadeIn(500);
    });
    $('#tvnimhg_record').fadeOut(500, function() {
        $(this).text(formArray[31].value).fadeIn(500);
    });
    $('#tvim_record').fadeOut(500, function() {
        $(this).text(formArray[32].value).fadeIn(500);
    });
    $('#nephrectomie_record').fadeOut(500, function() {
        $(this).text(formArray[33].value).fadeIn(500);
    });
    $('#orchidectomie_record').fadeOut(500, function() {
        $(this).text(formArray[34].value).fadeIn(500);
    });

    // Traitement

    $('#mmc_record').fadeOut(500, function() {
        $(this).text(formArray[35].value).fadeIn(500);
    });
    $('#bcg_record').fadeOut(500, function() {
      $(this).text(formArray[36].value).fadeIn(500);
    });
    $('#prostatectomie_record').fadeOut(500, function() {
        $(this).text(formArray[37].value).fadeIn(500);
    });
    $('#ana_lh_rh_record').fadeOut(500, function() {
        $(this).text(formArray[38].value).fadeIn(500);
    });
    $('#chimio_record').fadeOut(500, function() {
        $(this).text(formArray[39].value).fadeIn(500);
    });
    $('#antiandro_record').fadeOut(500, function() {
        $(this).text(formArray[40].value).fadeIn(500);
    });
    $('#cystopro_record').fadeOut(500, function() {
        $(this).text(formArray[41].value).fadeIn(500);
    });
    $('#bricker_record').fadeOut(500, function() {
        $(this).text(formArray[42].value).fadeIn(500);
    });
    $('#remp_vessie_record').fadeOut(500, function() {
        $(this).text(formArray[43].value).fadeIn(500);
    });
    $('#nephrec_record').fadeOut(500, function() {
        $(this).text(formArray[44].value).fadeIn(500);
    });
    $('#Orchidec_record').fadeOut(500, function() {
        $(this).text(formArray[45].value).fadeIn(500);
    });
    $('#urs_rigide_record').fadeOut(500, function() {
        $(this).text(formArray[46].value).fadeIn(500);
    });
    $('#urs_souple_record').fadeOut(500, function() {
        $(this).text(formArray[47].value).fadeIn(500);
    });
    $('#lithotripsie_record').fadeOut(500, function() {
        $(this).text(formArray[48].value).fadeIn(500);
    });

    // Diagnostic
    $('#prevention_record').fadeOut(500, function() {
        $(this).text(formArray[49].value).fadeIn(500);
    });
    /*$('#prescription_record').fadeOut(500, function() {
        $(this).text(form[8].value).fadeIn(500);
    });*/

    newval= ($('.visite').length)?parseInt($('.visite')[0].dataset.value,10)+1:0;
    //console.log(newval);
    $('.selected').toggleClass('selected');
    $("#historique>ul").prepend('<li class="visite selected" data-value="'+newval+'">'+date+'</li>');
    $('.selected').fadeOut(400,function(){
        $('.selected').fadeIn(400);
    });
    $('#print-link').fadeOut(500, function() {
        $(this).attr('href',"ordonnance?id="+newval).fadeIn(500);
    });

  });
});








    $('#welcome').fadeIn('fast', function() {
      $('#welcome h1').fadeIn(1000,function(){

          $(this).animate({
                  'marginTop': '2%'
                }, 'medium', function() {
                  // Animation complete.
                  $('#welcome h2').fadeIn(400, function(){
                    setTimeout(function() {
                      $('#welcome').fadeOut();
                    }, 1000);
                  });
              });
      });
    });

    $(document).on('change','#next-visit', function(event) {
      val=event.target.value
      //console.log(val);
      id=$('#id-patient').text();
      //console.log(id);

      $.post('/next-visit', {id:id,date: val}, function(data, textStatus, xhr) {
        if(data=='true') $('#saved').fadeOut('500', function() {
          $(this).text('Date enregistrée.').fadeIn(500, function() {

          }).delay(3000).fadeOut('500');
        });
      });


  });

    $(".dataTable.no-footer").attr({style: '' });
    $(".patients-index").fadeIn(400);
    $(".patients-form").fadeIn(400);
    $(".patients-view").fadeIn(400);

    $(document).on('click','.ajax-request', function(event) {
      event.preventDefault();

      $.post($(event.target).attr('href'), function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });

    $(document).on('click','.ajax-request-form', function(event) {
      event.preventDefault();
      post=$('.patients-form form').serializeArray();
      //console.log(post);
      $.post($(event.target).attr('href'), post, function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });

    $( document ).ajaxStart(function() {
      $( "#loading" ).fadeIn();
    });

    $( document ).ajaxStop(function() {
      $( "#loading" ).fadeOut();
    });

  });

  $(document).ajaxComplete(function(event) {
    //console.log(event.target);
    $(".dataTable.no-footer").attr({style: '' });
    $(".patients-index").fadeIn(400);
    $(".patients-form").fadeIn(400);
    $(".patients-view").fadeIn(400);
/*
    $(document).on('click','.ajax-request', function(event) {
      event.preventDefault();

      $.post($(event.target).attr('href'), function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });
    $(document).on('click','.ajax-request-form', function(event) {
      event.preventDefault();
      post=$('.patients-form form').serializeArray();
      //console.log(post);
      $.post($(event.target).attr('href'), post, function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });

/*
    $( document ).ajaxStart(function() {
      $( "#loading" ).show();
    });
    $( document ).ajaxStop(function() {
      $( "#loading" ).hide();
    });*/
  });

</script>
</html>
<?php $this->endPage() ?>
