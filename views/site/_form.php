<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Patients */
/* @var $form yii\widgets\ActiveForm */
?>
<hr>

<div class="patients-form collapse">

     <?php $form = ActiveForm::begin(/*['action'=> $model->isNewRecord ? '/site/create' : '/site/update']*/); ?>

    <?= $form->field($model, 'nom',['options'=>['class'=>'col-md-3']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prenom',['options'=>['class'=>'col-md-3']])->textInput(['maxlength' => true]) ?>

    <?php /* $form->field($model, 'naissance',['options'=>['class'=>'col-md-6']])->textInput() */?>

    <?= $form->field($model,'naissance',['options'=>['class'=>'col-md-3']])->widget(yii\jui\DatePicker::className(),['language'=>'fr_FR','dateFormat' => 'long','clientOptions' => ['changeYear'=> 'true','changeMonth' => 'true','yearRange' => '1900:2099'],'options'=>['class'=>'form-control']]) ?>

    <?= $form->field($model, 'genre',['options'=>['class'=>'col-md-3']])->dropDownlist($genres,['prompt'=>'-']) ?>

    <?= $form->field($model, 'tel',['options'=>['class'=>'col-md-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'profession',['options'=>['class'=>'col-md-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ville',['options'=>['class'=>'col-md-6']])->dropDownlist($wilayas,['prompt'=>'-']) ?>

    <?= $form->field($model, 'adresse',['options'=>['class'=>'col-md-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'antecedents_per',['options'=>['class'=>'col-md-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'antecedents_fam',['options'=>['class'=>'col-md-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description',['options'=>['class'=>'col-md-6']])->textInput(['maxlength' => true]) ?>

    <div class="form-group   pull-right">
      <br>
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Modifier', ['class' => $model->isNewRecord ? 'btn btn-success ajax-request-form' : 'btn btn-primary ajax-request-form','href' => $model->isNewRecord ? '/site/create-ajax' : '/site/update-ajax?id='.$model->id]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
