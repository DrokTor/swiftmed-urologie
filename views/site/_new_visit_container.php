

        <!-- visit form collaped -->
        <form id="new-visit-container"   class="row collapse">

          <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
            <p id="motif" class="new-visits  col-md-12">
              <textarea name="Visites[motif]" class="col-md-12"></textarea>
            </p>
          </div>
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroInterrogatoire">Interrogatoire <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroInterrogatoire" class="collapse col-md-12">
            <p id="hemodure" class="col-md-6"><label class="col-md-12">Hématurie:</label><br>
                <textarea name="UroInterrogatoire[hemodure]" class="col-md-12"></textarea>
            </p>
            <p id="douleur" class="col-md-6"><label class="col-md-12">Douleur lombaire:</label><br>
              <textarea name="UroInterrogatoire[douleur]" class="col-md-12"></textarea>
            </p>
            <p id="masse" class="col-md-6"><label class="col-md-12">Masse Rénale:</label><br>
                <textarea name="UroInterrogatoire[masse]" class="col-md-12"></textarea>
            </p>
            <p id="grosse" class="col-md-6"><label class="col-md-12">Grosse Bourse:</label><br>
              <textarea name="UroInterrogatoire[grosse]" class="col-md-12"></textarea>
            </p>
            <p id="psa" class="col-md-6"><label class="col-md-12">Elevation du PSA:</label><br>
                <textarea name="UroInterrogatoire[psa]" class="col-md-12"></textarea>
            </p>
            <p id="tuba" class="col-md-6"><label class="col-md-12">Tuba:</label><br>
              <textarea name="UroInterrogatoire[tuba]" class="col-md-12"></textarea>
            </p>
          </div>
           </div>
          <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
            <p id="examen-cli" class="new-visits  col-md-12">
              <textarea name="Visites[examen_clinique]" class="col-md-12"></textarea>
            </p>
          </div><!--
          <div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
            <p id="examen-bio" class="new-visits  col-md-12">
              <textarea name="examen_biologique" class="col-md-12"></textarea>
            </p>
          </div>--->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroBiologique">Examen biologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroBiologique" class="collapse col-md-12">
            <p id="uree" class="col-md-6"><label class="col-md-12">Urée:</label><br>
                <textarea name="UroBiologique[uree]" class="col-md-12"></textarea>
            </p>
            <p id="creat" class="col-md-6"><label class="col-md-12">Créat:</label><br>
              <textarea name="UroBiologique[creat]" class="col-md-12"></textarea>
            </p>
            <p id="mdrd" class="col-md-6"><label class="col-md-12">MDRD:</label><br>
                <textarea name="UroBiologique[mdrd]" class="col-md-12"></textarea>
            </p>
            <p id="biopsa" class="col-md-6"><label class="col-md-12">PSA:</label><br>
              <textarea name="UroBiologique[psa]" class="col-md-12"></textarea>
            </p>
            <p id="bhcg" class="col-md-6"><label class="col-md-12">BHCG:</label><br>
                <textarea name="UroBiologique[bhcg]" class="col-md-12"></textarea>
            </p>
            <p id="afp" class="col-md-6"><label class="col-md-12">αFP:</label><br>
              <textarea name="UroBiologique[afp]" class="col-md-12"></textarea>
            </p>
            <p id="ldh" class="col-md-6"><label class="col-md-12">LDH:</label><br>
                <textarea name="UroBiologique[ldh]" class="col-md-12"></textarea>
            </p>
            <p id="cytologie" class="col-md-6"><label class="col-md-12">Cytologie urinaire:</label><br>
              <textarea name="UroBiologique[cytologie]" class="col-md-12"></textarea>
            </p>
          </div>
        </div><!--
          <div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
            <p id="examen-rad" class="new-visits  col-md-12">
              <textarea name="examen_radiologique" class="col-md-12"></textarea>
            </p>
          </div>-->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroRadiologique">Examen Radiologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroRadiologique" class="collapse col-md-12">
            <p id="asp" class="col-md-6"><label class="col-md-12">ASP:</label><br>
                <textarea name="UroRadiologique[asp]" class="col-md-12"></textarea>
            </p>
            <p id="echo" class="col-md-6"><label class="col-md-12">Echo:</label><br>
              <textarea name="UroRadiologique[echo]" class="col-md-12"></textarea>
            </p>
            <p id="uroscanner" class="col-md-6"><label class="col-md-12">Uroscanner:</label><br>
                <textarea name="UroRadiologique[uroscanner]" class="col-md-12"></textarea>
            </p>
            <p id="uroirm" class="col-md-6"><label class="col-md-12">UroIRM:</label><br>
              <textarea name="UroRadiologique[uroirm]" class="col-md-12"></textarea>
            </p>
            <p id="densite" class="col-md-6"><label class="col-md-12">Densité:</label><br>
                <textarea name="UroRadiologique[densite]" class="col-md-12"></textarea>
            </p>

          </div>
           </div>
          <!--new exploration_fonctionnelle
          <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
            <p id="exploration" class="new-visits  col-md-12">
              <textarea name="exploration_fonctionnelle" class="col-md-12"></textarea>
            </p>
           </div>-->
           <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroExploration">Exploration fonctionnelle <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>

             <div id="UroExploration" class="collapse col-md-12">
             <p id="debimetrie" class="col-md-6"><label class="col-md-12">Débimetrie:</label><br>
                 <textarea name="UroExploration[debimetrie]" class="col-md-12"></textarea>
             </p>
             <p id="cystomano" class="col-md-6"><label class="col-md-12">Cystomanometrie:</label><br>
               <textarea name="UroExploration[cystomano]" class="col-md-12"></textarea>
             </p>

           </div>
         </div><!--
          <div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
            <p id="diagnostic" class="new-visits  col-md-12">
              <textarea name="diagnostic" class="col-md-12"></textarea>
            </p>
          </div>-->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroDiagnostique">Diagnostic <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroDiagnostique" class="collapse col-md-12">
            <p id="tvs" class="col-md-6"><label class="col-md-12">TV Superficielle:</label><br>
                <textarea name="UroDiagnostique[tvs]" class="col-md-12"></textarea>
            </p>
            <p id="tvi" class="col-md-6"><label class="col-md-12">TV Infiltrante:</label><br>
              <textarea name="UroDiagnostique[tvi]" class="col-md-12"></textarea>
            </p>
            <p id="cr_prostate" class="col-md-6"><label class="col-md-12">Cancer de prostate:</label><br>
                <textarea name="UroDiagnostique[cr_prostate]" class="col-md-12"></textarea>
            </p>
            <p id="cr_rein" class="col-md-6"><label class="col-md-12">Cancer du rein:</label><br>
              <textarea name="UroDiagnostique[cr_rein]" class="col-md-12"></textarea>
            </p>
            <p id="cr_testicule" class="col-md-6"><label class="col-md-12">Cancer du testicule:</label><br>
                <textarea name="UroDiagnostique[cr_testicule]" class="col-md-12"></textarea>
            </p>
            <p id="li_renale" class="col-md-6"><label class="col-md-12">Lithiase rénale:</label><br>
              <textarea name="UroDiagnostique[li_renale]" class="col-md-12"></textarea>
            </p>
            <p id="li_ureterale" class="col-md-6"><label class="col-md-12">Lithiase uréterale:</label><br>
              <textarea name="UroDiagnostique[li_ureterale]" class="col-md-12"></textarea>
            </p>
          </div>
           </div>



           <!-- Anapath -->
           <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroAnapath">Anapath <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
             <!---<p id="interrogatoire" class="new-visits  col-md-12">
               <textarea name="interogatoire" class="col-md-12"></textarea>


             </p>--->
             <div id="UroAnapath" class="collapse col-md-12">
             <p id="tvnimbg" class="col-md-6"><label class="col-md-12">TVNIM bas grade:</label><br>
                 <textarea name="UroAnapath[tvnimbg]" class="col-md-12"></textarea>
             </p>
             <p id="tvnimhg" class="col-md-6"><label class="col-md-12">TVNIM haut grade:</label><br>
               <textarea name="UroAnapath[tvnimhg]" class="col-md-12"></textarea>
             </p>
             <p id="tvim" class="col-md-6"><label class="col-md-12">TVIM:</label><br>
                 <textarea name="UroAnapath[tvim]" class="col-md-12"></textarea>
             </p>
             <p id="nephrectomie" class="col-md-6"><label class="col-md-12">Néphrectomie:</label><br>
               <textarea name="UroAnapath[nephrectomie]" class="col-md-12"></textarea>
             </p>
             <p id="orchidectomie" class="col-md-6"><label class="col-md-12">Orchidectomie:</label><br>
                 <textarea name="UroAnapath[orchidectomie]" class="col-md-12"></textarea>
             </p>

           </div>
            </div>

          <!--<div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
            <p id="prescription" class="new-visits  col-md-12">
              <textarea name="prescription" class="col-md-12"></textarea>
            </p>
          </div>-->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroTraitement">Traitement <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroTraitement" class="collapse col-md-12">
            <p id="mmc" class="col-md-6"><label class="col-md-12">MMC:</label><br>
                <textarea name="UroTraitement[mmc]" class="col-md-12"></textarea>
            </p>
            <p id="bcg" class="col-md-6"><label class="col-md-12">BCG:</label><br>
              <textarea name="UroTraitement[bcg]" class="col-md-12"></textarea>
            </p>
            <p id="prostatectomie" class="col-md-6"><label class="col-md-12">Prostatectomie:</label><br>
                <textarea name="UroTraitement[prostatectomie]" class="col-md-12"></textarea>
            </p>
            <p id="ana_lh_rh" class="col-md-6"><label class="col-md-12">Analogues LH-RH:</label><br>
              <textarea name="UroTraitement[ana_lh_rh]" class="col-md-12"></textarea>
            </p>
            <p id="chimio" class="col-md-6"><label class="col-md-12">Chimiotherapie:</label><br>
                <textarea name="UroTraitement[chimio]" class="col-md-12"></textarea>
            </p>
            <p id="antiandro" class="col-md-6"><label class="col-md-12">Antiandrogenes:</label><br>
              <textarea name="UroTraitement[antiandro]" class="col-md-12"></textarea>
            </p>
            <p id="cystopro" class="col-md-6"><label class="col-md-12">Cystoprostatectomie:</label><br>
                <textarea name="UroTraitement[cystopro]" class="col-md-12"></textarea>
            </p>
            <p id="bricker" class="col-md-6"><label class="col-md-12">Bricker:</label><br>
              <textarea name="UroTraitement[bricker]" class="col-md-12"></textarea>
            </p>
            <p id="remp_vessie" class="col-md-6"><label class="col-md-12">Remplacement de vessie:</label><br>
              <textarea name="UroTraitement[remp_vessie]" class="col-md-12"></textarea>
            </p>
            <p id="nephrec" class="col-md-6"><label class="col-md-12">Néphrectomie:</label><br>
              <textarea name="UroTraitement[nephrec]" class="col-md-12"></textarea>
            </p>
            <p id="orchidec" class="col-md-6"><label class="col-md-12">Orchidectomie:</label><br>
              <textarea name="UroTraitement[orchidec]" class="col-md-12"></textarea>
            </p>
            <p id="urs_rigide" class="col-md-6"><label class="col-md-12">URS rigide:</label><br>
              <textarea name="UroTraitement[urs_rigide]" class="col-md-12"></textarea>
            </p>
            <p id="urs_souple" class="col-md-6"><label class="col-md-12">URS souple:</label><br>
              <textarea name="UroTraitement[urs_souple]" class="col-md-12"></textarea>
            </p>
            <p id="lithotripsie" class="col-md-6"><label class="col-md-12">Lithotripsie:</label><br>
              <textarea name="UroTraitement[lithotripsie]" class="col-md-12"></textarea>
            </p>
          </div>
          </div>

          <!-- Prévention -->
          <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
            <p id="prevention" class="new-visits  col-md-12">
              <textarea name="Visites[prevention]" class="col-md-12"></textarea>
            </p>
           </div>


           <button id="save-visit" class="row collapse btn btn-success pull-right"   >Enregistrer la visite</button>
         </form>

      <!-- visit form -->
