
          <div id="visit-container" class="row collapse">
            <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
              <p id="motif_record" class="old-visits  col-md-12">
               </p>
            </div>
            <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroInterrogatoire_record">Interrogatoire <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
              <!---<p id="interrogatoire" class="new-visits  col-md-12">
                <textarea name="interogatoire" class="col-md-12"></textarea>


              </p>--->
              <div id="UroInterrogatoire_record" class="collapse col-md-12">
              <div class="col-md-6">
                <label class="col-md-12">Hématurie:</label><br>
              <p id="hemodure_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Douleur lombaire:</label><br>
              <p id="douleur_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Masse Rénale:</label><br>
              <p id="masse_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Grosse Bourse:</label><br>
              <p id="grosse_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Elevation du PSA:</label><br>
              <p id="psa_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Tuba:</label><br>
              <p id="tuba_record" class="col-md-6">
               </p>
             </div>
            </div>
             </div>
             </div>
             <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
              <p id="exam_cli_record" class="old-visits  col-md-12">
               </p>
            </div>
            <!--<div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
              <p id="exam_bio_record" class="old-visits  col-md-12">
               </p>
             </div>--->
             <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroBiologique_record">Examen biologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
               <!---<p id="interrogatoire" class="new-visits  col-md-12">
                 <textarea name="interogatoire" class="col-md-12"></textarea>


               </p>--->
               <div id="UroBiologique_record" class="collapse col-md-12">
                 <div class="col-md-6">
                   <label class="col-md-12">Urée:</label><br>
                 <p id="uree_record" class="col-md-6">
               </p>

              </div>
              <div class="col-md-6">
                 <label class="col-md-12">Créat:</label><br>
               <p id="creat_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">MDRD:</label><br>
               <p id="mdrd_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">PSA:</label><br>
               <p id="biopsa_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">BHCG:</label><br>
               <p id="bhcg_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">αFP:</label><br>
               <p id="afp_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">LDH:</label><br>
               <p id="ldh_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">Cytologie urinaire:</label><br>
               <p id="cytologie_record" class="col-md-6">
               </p>
             </div>
             </div>
              </div>
           </div>
           <div class="row container-fluid">
            <!---<div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
              <p id="exam_radio_record" class="old-visits  col-md-12">
               </p>
            </div>-->
            <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroRadiologique_record">Examen Radiologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
              <!---<p id="interrogatoire" class="new-visits  col-md-12">
                <textarea name="interogatoire" class="col-md-12"></textarea>


              </p>--->
              <div id="UroRadiologique_record" class="collapse col-md-12">
              <div class="col-md-6">
                <label class="col-md-12">ASP:</label><br>
              <p id="asp_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Echo:</label><br>
              <p id="echo_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">UroScanner:</label><br>
              <p id="uroscanner_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">UroIRM:</label><br>
              <p id="uroirm_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Densité:</label><br>
              <p id="densite_record" class="col-md-6">
               </p>
             </div>

            </div>
             </div>

            <!--new exploration_fonctionnelle
            <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
              <p id="exploration_record" class="old-visits  col-md-12">
               </p>
             </div>-->
             <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroExploration_record">Exploration fonctionnelle <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>

               <div id="UroExploration_record" class="collapse col-md-12">
                 <div class="col-md-6">
                   <label class="col-md-12">Débimetrie:</label><br>
               <p id="debimetrie_record" class="col-md-6">
                </p>
              </div>
                <div class="col-md-6">
                  <label class="col-md-12">Cystomanometrie:</label><br>
               <p id="cystomano_record" class="col-md-6">
                </p>
              </div>

             </div>
              </div>
           </div>
           <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroDiagnostique_record">Diagnostic <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
             <!---<p id="interrogatoire" class="new-visits  col-md-12">
               <textarea name="interogatoire" class="col-md-12"></textarea>


             </p>--->
             <div id="UroDiagnostique_record" class="collapse col-md-12">
             <div class="col-md-6">
               <label class="col-md-12">TV Superficielle:</label><br>
             <p id="tvs_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">TV Infiltrante:</label><br>
             <p id="tvi_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Cancer de prostate:</label><br>
             <p id="cr_prostate_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Cancer du rein:</label><br>
             <p id="cr_rein_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Cancer du testicule:</label><br>
             <p id="cr_testicule_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Lithiase rénale:</label><br>
             <p id="li_renale_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Lithiase uréterale:</label><br>
             <p id="li_ureterale_record" class="col-md-6">
              </p>
            </div>

           </div>
            </div>



           <!-- anapath -->
           <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroAnapath_record">Anapath <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
             <!---<p id="interrogatoire" class="new-visits  col-md-12">
               <textarea name="interogatoire" class="col-md-12"></textarea>


             </p>--->
             <div id="UroAnapath_record" class="collapse col-md-12">
             <div class="col-md-6">
               <label class="col-md-12">TVNIM bas grade:</label><br>
             <p id="tvnimbg_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">TVNIM haut grade:</label><br>
             <p id="tvnimhg_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">TVIM:</label><br>
             <p id="tvim_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Néphrectomie:</label><br>
             <p id="nephrectomie_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Orchidectomie:</label><br>
             <p id="orchidectomie_record" class="col-md-6">
              </p>
            </div>

           </div>
            </div>
           <!--
           <div class="row container-fluid">
             <div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
               <p id="prescription_record" class="old-visits  col-md-12">
                </p>
              </div>
            </div> -->
            <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroTraitement_record">Traitement <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
              <!---<p id="interrogatoire" class="new-visits  col-md-12">
                <textarea name="interogatoire" class="col-md-12"></textarea>


              </p>--->
              <div id="UroTraitement_record" class="collapse col-md-12">
              <div class="col-md-6">
                <label class="col-md-12">MMC:</label><br>
              <p id="mmc_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">BCG:</label><br>
              <p id="bcg_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Prostatectomie:</label><br>
              <p id="prostatectomie_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Analogues LH-RH:</label><br>
              <p id="ana_lh_rh_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Chimiotherapie:</label><br>
              <p id="chimio_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Antiandrogenes:</label><br>
              <p id="antiandro_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Cystoprostatectomie:</label><br>
              <p id="cystopro_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Bricker:</label><br>
              <p id="bricker_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Remplacement de vessie:</label><br>
              <p id="remp_vessie_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Néphrectomie:</label><br>
              <p id="nephrec_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Orchidectomie:</label><br>
              <p id="Orchidec_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">URS rigide:</label><br>
              <p id="urs_rigide_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">URS souple:</label><br>
              <p id="urs_souple_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Lithotripsie:</label><br>
              <p id="lithotripsie_record" class="col-md-6">
               </p>
             </div>

            </div>
             </div>

             <div class="row container-fluid">
              <!---<div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
                <p id="diagnostic_record" class="old-visits  col-md-12">
                 </p>
              </div>-->
              <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
                <p id="prevention_record" class="old-visits  col-md-12">
                 </p>
               </div>
             </div>

            </div>
