<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Patients */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;
//dd($visites[0]->explo);

?>

<div class="col-md-3">
<h3>Historique du Patient</h3>
<div  id="historique">
<ul>
<?php

  foreach ($visites as $key => $value) {
    echo '<li class="visite '.(($key=='0')?'selected':$key).'" data-value="'.$value->id.'">'.$formatter->asDate($value->date_visite,'long')."</li>";
  }

 ?>
</ul>
</div>
</div>
<div class="patients-view col-md-9 collapse">


    <div class="row">
    <p class="col-md-5">
        <?= Html::a('Modifier les informations du patient', ['update-ajax', 'id' => $model->id], ['class' => 'btn btn-primary ajax-request']) ?>
        <?php /* Html::a('Supprimer', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'êtes vous sûr de vouloir supprimer ce Patient?',
                'method' => 'post',
            ],
        ]) */?>
    </p>
    <p class="text-primary col-md-4">
      Prochaine visite programmée pour le :
    </p>
    <p class="next-visit-container col-md-3">
    <?= yii\jui\DatePicker::widget(['name' => 'next_visit','value' => ($model->next_visit)?$model->next_visit:'','language'=>'fr_FR','dateFormat' => 'long','id'=>'next-visit']) ?>
    <br><span id="saved" class="text-success">&nbsp</span>
    </p>
  </div>
    <hr>

    <?= $this->render('_patient_info',['model'=>$model,'age'=>$age]) ?>

     <hr>
     <div class="container-fluid">
       <h4  >
         <span class="visite_heading" >Date de la dernière visite:</span>
          &nbsp <label id="date-visite" class="text-success"> &nbsp
            <?php  echo isset($visites[0])? $formatter->asDate($visites[0]->date_visite,'long'):' - Historique du patient vide.'; ?>
          </label>
          <!--<a id="print-link" target="_blank" href="ordonnance?id=<?php if(isset($visites[0])) echo ($visites[0]['id']) ; ?>" <?php if(!isset($visites['0'])) { echo 'class="collapse"'; } ?> ><i class=" print pull-right glyphicon glyphicon-print"></i>
          </a>-->
        </h4>
        <button id="new-visit" class="btn btn-primary pull-right">Nouvelle visite</button>
       </div>
       <?php if(isset($visites['0'])) { ?>

         <?= $this->render('_visit_record',['visites'=>$visites]) ?>


        <?php }else{ ?>

          <?= $this->render('_visit_blank') ?>

        <?php } ?>


        <?= $this->render('_new_visit_container') ?>

      <!-- visit form -->
      <p class="sponsor-visite"> Sponsorisé par <br><br><img  src="/images/sponsor.png" /></p>

</div>
