<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Patients */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;
//dd($visites[0]->explo);

?>

<div class="col-md-3">
<h3>Historique du Patient</h3>
<div  id="historique">
<ul>
<?php

  foreach ($visites as $key => $value) {
    echo '<li class="visite '.(($key=='0')?'selected':$key).'" data-value="'.$value->id.'">'.$formatter->asDate($value->date_visite,'long')."</li>";
  }

 ?>
</ul>
</div></div>
<div class="patients-view col-md-9 collapse">


    <div class="row">
    <p class="col-md-5">
        <?= Html::a('Modifier les informations du patient', ['update-ajax', 'id' => $model->id], ['class' => 'btn btn-primary ajax-request']) ?>
        <?php /* Html::a('Supprimer', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'êtes vous sûr de vouloir supprimer ce Patient?',
                'method' => 'post',
            ],
        ]) */?>
    </p>
    <p class="text-primary col-md-4">
      Prochaine visite programmée pour le :
    </p>
    <p class="next-visit-container col-md-3">
    <?= yii\jui\DatePicker::widget(['name' => 'next_visit','value' => ($model->next_visit)?$model->next_visit:'','language'=>'fr_FR','dateFormat' => 'long','id'=>'next-visit']) ?>
    <br><span id="saved" class="text-success">&nbsp</span>
    </p>
  </div>
    <hr>
    <?php

      echo '<div class="container-fluid"><div class="col-md-3"><label>Identifiant: </label><br>
      <label>Genre: </label>
      </div>';

      echo '<div class="info col-md-3">
        <span id="id-patient">'.$model->id.'</span><br>
        <span>'.$model->sex->nom.' ( '.$age.' )</span><br></div>';

      echo '<div class="col-md-3">
        <label>Nom: </label><br>
        <label>Prénom: </label><br>
        </div>';

        echo '<div class="info col-md-3">
          <span>'.$model->nom.'</span><br>
          <span>'.$model->prenom.'</span><br></div></div>';

      echo ' <div class="row pull-right" data-toggle="collapse" data-target="#more"><span class="drop-menu text-primary">Voir plus</span></div>
            <br>
            <div id="more" class=" collapse">';

      echo '<br><div class="container-fluid "><div class="col-md-3">
      <label>Tel: </label><br>

      </div>';

      echo '<div class="info col-md-3">
        <span>'.$model->tel.'</span><br>
        </div>';

        echo '<div class="col-md-3">
        <label>Profession: </label><br>

        </div>';

      echo '<div class="info col-md-3">
        <span>'.$model->profession.'</span><br></div>

        </div>';

      echo '<br><div class="container-fluid "><div class="col-md-3">
      <label>Ville: </label><br>

      </div>';

      echo '<div class="info col-md-3">
        <span>'.$model->wilaya->nom.'</span><br>
         </div>';



        echo '<div class="col-md-3">
        <label>Adresse: </label><br>

        </div>';
      echo '<div class="info col-md-3">
        <span>'.$model->adresse.'</span><br></div>
        </div>';

        echo '<br><div class="container-fluid "><div class="col-md-3">
        <label>Antécedents personnels:</label><br>

        </div>';

        echo '<div class="info col-md-3">
          <span>'.$model->antecedents_per.'</span><br>
           </div> ';

        echo ' <div class="col-md-3">
         <label>Antécedents familiaux: </label><br>

        </div>';

        echo '<div class="info col-md-3">
           <span>'.$model->antecedents_fam.'</span><br>
          </div></div>';

        echo '<br><div class="container-fluid "><div class="col-md-3">
        <label>Note: </label><br>

        </div>';
      echo '<div class="info col-md-9">
        <span>'.$model->description.'</span><br>
        </div></div>
        </div>';

     ?>

     <hr>
     <div class="container-fluid">
       <h4  >
         <span class="visite_heading" >Date de la dernière visite:</span>
          &nbsp <label id="date-visite" class="text-success"> &nbsp
            <?php  echo isset($visites[0])? $formatter->asDate($visites[0]->date_visite,'long'):' - Historique du patient vide.'; ?>
          </label>
          <a id="print-link" target="_blank" href="ordonnance?id=<?php if(isset($visites[0])) echo ($visites[0]['id']) ; ?>" <?php if(!isset($visites['0'])) { echo 'class="collapse"'; } ?> ><i class=" print pull-right glyphicon glyphicon-print"></i>
          </a>
        </h4>
        <button id="new-visit" class="btn btn-primary pull-right">Nouvelle visite</button>
       </div>
       <?php if(isset($visites['0'])) { ?>
     <div id="visit-container" class="row">

       <div class="row container-fluid">
       <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
         <p id="motif_record" class="old-visits  col-md-12">
           <?= $visites['0']->motif ?>
         </p>
       </div>
       <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroInterrogatoire_record">Interrogatoire <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
         <!---<p id="interrogatoire" class="new-visits  col-md-12">
           <textarea name="interogatoire" class="col-md-12"></textarea>


         </p>--->
         <div id="UroInterrogatoire_record" class="collapse col-md-12">
         <div class="col-md-6"><label class="col-md-12">Hématurie:</label><br>
          <p id="hemodure_record" class="col-md-6">
           <?= $visites['0']->interro->hemodure ?>
          </p>
        </div>
         <div class="col-md-6"><label class="col-md-12">Douleur lombaire:</label><br>
          <p id="douleur_record" class="col-md-6">
           <?= $visites['0']->interro->douleur ?>
          </p>
        </div>
         <div class="col-md-6"><label class="col-md-12">Masse Rénale:</label><br>
          <p id="masse_record" class="col-md-6">
           <?= $visites['0']->interro->masse ?>
          </p>
        </div>
         <div class="col-md-6"><label class="col-md-12">Grosse Bourse:</label><br>
         <p id="grosse_record" class="col-md-6">
           <?= $visites['0']->interro->grosse ?>
          </p>
        </div>
        <div class="col-md-6"> <label class="col-md-12">Elevation du PSA:</label><br>
          <p id="psa_record" class="col-md-6">
           <?= $visites['0']->interro->psa ?>
          </p>
        </div>
        <div class="col-md-6"><label class="col-md-12">Tuba:</label><br>
          <p id="tuba_record" class="col-md-6">
           <?= $visites['0']->interro->tuba ?>
          </p>
        </div>
       </div>
        </div>
       </div>
       <div class="row container-fluid">
       <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
         <p id="exam_cli_record" class="old-visits  col-md-12">
           <?= $visites['0']->examen_clinique ?>
         </p>
       </div><!---
       <div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
         <p id="exam_bio_record" class="old-visits  col-md-12">
           <?= $visites['0']->examen_biologique ?>
         </p>
       </div>--->
      </div>
      <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroBiologique_record">Examen biologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
        <!---<p id="interrogatoire" class="new-visits  col-md-12">
          <textarea name="interogatoire" class="col-md-12"></textarea>


        </p>--->
        <div id="UroBiologique_record" class="collapse col-md-12">
          <div class="col-md-6">
          <label class="col-md-12">Urée:</label><br>
          <p id="uree_record" class="col-md-6">
            <?= $visites['0']->bio->uree ?>
        </p>
      </div>
        <div class="col-md-6">
        <label class="col-md-12">Créat:</label><br>
        <p id="creat_record" class="col-md-6">
          <?= $visites['0']->bio->creat ?>
        </p>
      </div>
        <div class="col-md-6">
        <label class="col-md-12">MDRD:</label><br>
        <p id="mdrd_record" class="col-md-6">
          <?= $visites['0']->bio->mdrd ?>
        </p>
      </div>
        <div class="col-md-6">
        <label class="col-md-12">PSA:</label><br>
        <p id="biopsa_record" class="col-md-6">
          <?= $visites['0']->bio->psa ?>
        </p>
      </div>
        <div class="col-md-6">
        <label class="col-md-12">BHCG:</label><br>
        <p id="bhcg_record" class="col-md-6">
          <?= $visites['0']->bio->bhcg ?>
        </p>
      </div>
        <div class="col-md-6">
        <label class="col-md-12">αFP:</label><br>
        <p id="afp_record" class="col-md-6">
          <?= $visites['0']->bio->afp ?>
        </p>
      </div>
        <div class="col-md-6">
        <label class="col-md-12">LDH:</label><br>
        <p id="ldh_record" class="col-md-6">
          <?= $visites['0']->bio->ldh ?>
        </p>
      </div>
        <div class="col-md-6">
        <label class="col-md-12">Cytologie urinaire:</label><br>
        <p id="cytologie_record" class="col-md-6">
          <?= $visites['0']->bio->cytologie ?>
        </p>
      </div>
      </div>
       </div>

       <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroRadiologique_record">Examen Radiologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
         <!---<p id="interrogatoire" class="new-visits  col-md-12">
           <textarea name="interogatoire" class="col-md-12"></textarea>


         </p>--->
         <div id="UroRadiologique_record" class="collapse col-md-12">
         <div class="col-md-6"><label class="col-md-12">ASP:</label><br>
          <p id="asp_record" class="col-md-6">
           <?= $visites['0']->radio->asp ?>
          </p>
        </div>
         <div class="col-md-6"><label class="col-md-12">Echo:</label><br>
          <p id="echo_record" class="col-md-6">
           <?= $visites['0']->radio->echo ?>
          </p>
        </div>
         <div class="col-md-6"><label class="col-md-12">UroScanner:</label><br>
          <p id="uroscanner_record" class="col-md-6">
           <?= $visites['0']->radio->uroscanner ?>
          </p>
        </div>
         <div class="col-md-6"><label class="col-md-12">UroIRM:</label><br>
         <p id="uroirm_record" class="col-md-6">
           <?= $visites['0']->radio->uroirm ?>
          </p>
        </div>
        <div class="col-md-6"> <label class="col-md-12">Densite:</label><br>
          <p id="densite_record" class="col-md-6">
           <?= $visites['0']->radio->densite ?>
          </p>
        </div>

       </div>
        </div>

      <div class="row container-fluid">

       <!---<div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
         <p id="exam_radio_record" class="old-visits  col-md-12">
           <?= $visites['0']->examen_radiologique ?>
         </p>
       </div>--->
       <!--new exploration_fonctionnelle
       <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
         <p id="exploration_record" class="old-visits  col-md-12">
           <?= $visites['0']->exploration_fonctionnelle ?>
         </p>
        </div>-->
        </div>
        <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroExploration_record">Exploration fonctionnelle <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>

          <div id="UroExploration_record" class="collapse col-md-12">
          <div class="col-md-6"><label class="col-md-12">Débimetrie:</label><br>
          <p id="debimetrie_record" class="col-md-6">
                <?= $visites['0']->explo->debimetrie ?>
          </p>
        </div>
          <div class="col-md-6"><label class="col-md-12">Cystomanometrie:</label><br>
          <p id="cystomano_record" class="col-md-6">
              <?= $visites['0']->explo->cystomano ?>
          </p>
        </div>

        </div>
         </div>

         <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroDiagnostique_record">Diagnostic <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
           <!---<p id="interrogatoire" class="new-visits  col-md-12">
             <textarea name="interogatoire" class="col-md-12"></textarea>


           </p>--->
           <div id="UroDiagnostique_record" class="collapse col-md-12">
           <div class="col-md-6"><label class="col-md-12">TV Superficielle:</label><br>
            <p id="tvs_record" class="col-md-6">
             <?= $visites['0']->diagno->tvs ?>
            </p>
          </div>
           <div class="col-md-6"><label class="col-md-12">TV Infiltrante:</label><br>
            <p id="tvi_record" class="col-md-6">
             <?= $visites['0']->diagno->tvi ?>
            </p>
          </div>
           <div class="col-md-6"><label class="col-md-12">Cancer de prostate:</label><br>
            <p id="cr_prostate_record" class="col-md-6">
             <?= $visites['0']->diagno->cr_prostate ?>
            </p>
          </div>
           <div class="col-md-6"><label class="col-md-12">Cancer du rein:</label><br>
           <p id="cr_rein_record" class="col-md-6">
             <?= $visites['0']->diagno->cr_rein ?>
            </p>
          </div>
          <div class="col-md-6"> <label class="col-md-12">Cancer du testicule:</label><br>
            <p id="cr_testicule_record" class="col-md-6">
             <?= $visites['0']->diagno->cr_testicule ?>
            </p>
          </div>
          <div class="col-md-6"> <label class="col-md-12">lithiase rénale:</label><br>
            <p id="li_renale_record" class="col-md-6">
             <?= $visites['0']->diagno->li_renale ?>
            </p>
          </div>
          <div class="col-md-6"> <label class="col-md-12">lithiase uréterale:</label><br>
            <p id="li_ureterale_record" class="col-md-6">
             <?= $visites['0']->diagno->li_ureterale ?>
            </p>
          </div>

         </div>
          </div>

        <div class="row container-fluid">
       <!--<div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
         <p id="diagnostic_record" class="old-visits  col-md-12">
           <?= $visites['0']->diagnostic ?>
         </p>
       </div>-->
       <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
         <p id="prevention_record" class="old-visits  col-md-12">
           <?= $visites['0']->prevention ?>
         </p>
        </div>
      </div><!--
        <div class="row container-fluid">
        <div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
          <p id="prescription_record" class="old-visits  col-md-12">
            <?= $visites['0']->prescription ?>
          </p>
         </div>
       </div>-->


        <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroTraitement_record">Traitement <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
          <!---<p id="interrogatoire" class="new-visits  col-md-12">
            <textarea name="interogatoire" class="col-md-12"></textarea>


          </p>--->
          <div id="UroTraitement_record" class="collapse col-md-12">
          <div class="col-md-6"><label class="col-md-12">MMC:</label><br>
           <p id="mmc_record" class="col-md-6">
            <?= $visites['0']->traitement->mmc ?>
           </p>
         </div>
          <div class="col-md-6"><label class="col-md-12">BCG:</label><br>
           <p id="bcg_record" class="col-md-6">
            <?= $visites['0']->traitement->bcg ?>
           </p>
         </div>
          <div class="col-md-6"><label class="col-md-12">Prostatectomie:</label><br>
           <p id="prostatectomie_record" class="col-md-6">
            <?= $visites['0']->traitement->prostatectomie ?>
           </p>
         </div>
          <div class="col-md-6"><label class="col-md-12">Analogues LH-RH:</label><br>
          <p id="ana_lh_rh_record" class="col-md-6">
            <?= $visites['0']->traitement->ana_lh_rh ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Chimiotherapie:</label><br>
           <p id="chimio_record" class="col-md-6">
            <?= $visites['0']->traitement->chimio ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Antiandrogenes:</label><br>
           <p id="antiandro_record" class="col-md-6">
            <?= $visites['0']->traitement->antiandro ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Cystoprostatectomie:</label><br>
           <p id="cystopro_record" class="col-md-6">
            <?= $visites['0']->traitement->cystopro ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Bricker:</label><br>
           <p id="bricker_record" class="col-md-6">
            <?= $visites['0']->traitement->bricker ?>
           </p>
         </div>

         <div class="col-md-6"> <label class="col-md-12">Remplacement de vessie:</label><br>
           <p id="remp_vessie_record" class="col-md-6">
            <?= $visites['0']->traitement->remp_vessie ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Néphrectomie:</label><br>
           <p id="nephrec_record" class="col-md-6">
            <?= $visites['0']->traitement->nephrec ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Orchidectomie:</label><br>
           <p id="orchidec_record" class="col-md-6">
            <?= $visites['0']->traitement->orchidec ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">URS rigide:</label><br>
           <p id="urs_rigide_record" class="col-md-6">
            <?= $visites['0']->traitement->urs_rigide ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">URS souple:</label><br>
           <p id="urs_souple_record" class="col-md-6">
            <?= $visites['0']->traitement->urs_souple ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Lithotripsie:</label><br>
           <p id="lithotripsie_record" class="col-md-6">
            <?= $visites['0']->traitement->lithotripsie ?>
           </p>
         </div>

        </div>
         </div>


        <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroAnapath_record">Anapath <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
          <!---<p id="interrogatoire" class="new-visits  col-md-12">
            <textarea name="interogatoire" class="col-md-12"></textarea>


          </p>--->
          <div id="UroAnapath_record" class="collapse col-md-12">
          <div class="col-md-6"><label class="col-md-12">TVNIM bas grade:</label><br>
           <p id="tvnimbg_record" class="col-md-6">
            <?= $visites['0']->anapat->tvnimbg ?>
           </p>
         </div>
          <div class="col-md-6"><label class="col-md-12">TVNIM haut grade:</label><br>
           <p id="tvnimhg_record" class="col-md-6">
            <?= $visites['0']->anapat->tvnimhg ?>
           </p>
         </div>
          <div class="col-md-6"><label class="col-md-12">TVIM:</label><br>
           <p id="tvim_record" class="col-md-6">
            <?= $visites['0']->anapat->tvim ?>
           </p>
         </div>
          <div class="col-md-6"><label class="col-md-12">Néphrectomie:</label><br>
          <p id="nephrectomie_record" class="col-md-6">
            <?= $visites['0']->anapat->nephrectomie ?>
           </p>
         </div>
         <div class="col-md-6"> <label class="col-md-12">Orchidectomie:</label><br>
           <p id="orchidectomie_record" class="col-md-6">
            <?= $visites['0']->anapat->orchidectomie ?>
           </p>
         </div>

        </div>
         </div>

        </div>
        <?php }else{ ?>
          <div id="visit-container" class="row collapse">
            <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
              <p id="motif_record" class="old-visits  col-md-12">
               </p>
            </div>
            <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroInterrogatoire_record">Interrogatoire <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
              <!---<p id="interrogatoire" class="new-visits  col-md-12">
                <textarea name="interogatoire" class="col-md-12"></textarea>


              </p>--->
              <div id="UroInterrogatoire_record" class="collapse col-md-12">
              <div class="col-md-6">
                <label class="col-md-12">Hématurie:</label><br>
              <p id="hemodure_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Douleur lombaire:</label><br>
              <p id="douleur_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Masse Rénale:</label><br>
              <p id="masse_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Grosse Bourse:</label><br>
              <p id="grosse_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Elevation du PSA:</label><br>
              <p id="psa_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Tuba:</label><br>
              <p id="tuba_record" class="col-md-6">
               </p>
             </div>
            </div>
             </div>
             </div>
             <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
              <p id="exam_cli_record" class="old-visits  col-md-12">
               </p>
            </div>
            <div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
              <p id="exam_bio_record" class="old-visits  col-md-12">
               </p>
             </div>
             <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroBiologique_record">Examen biologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
               <!---<p id="interrogatoire" class="new-visits  col-md-12">
                 <textarea name="interogatoire" class="col-md-12"></textarea>


               </p>--->
               <div id="UroBiologique_record" class="collapse col-md-12">
                 <div class="col-md-6">
                   <label class="col-md-12">Urée:</label><br>
                 <p id="uree_record" class="col-md-6">
               </p>

              </div>
              <div class="col-md-6">
                 <label class="col-md-12">Créat:</label><br>
               <p id="creat_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">MDRD:</label><br>
               <p id="mdrd_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">PSA:</label><br>
               <p id="biopsa_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">BHCG:</label><br>
               <p id="bhcg_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">αFP:</label><br>
               <p id="afp_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">LDH:</label><br>
               <p id="ldh_record" class="col-md-6">
               </p>
             </div>
               <div class="col-md-6">
                 <label class="col-md-12">Cytologie urinaire:</label><br>
               <p id="cytologie_record" class="col-md-6">
               </p>
             </div>
             </div>
              </div>
           </div>
           <div class="row container-fluid">
            <!---<div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
              <p id="exam_radio_record" class="old-visits  col-md-12">
               </p>
            </div>-->
            <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroRadiologique_record">Examen Radiologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
              <!---<p id="interrogatoire" class="new-visits  col-md-12">
                <textarea name="interogatoire" class="col-md-12"></textarea>


              </p>--->
              <div id="UroRadiologique_record" class="collapse col-md-12">
              <div class="col-md-6">
                <label class="col-md-12">ASP:</label><br>
              <p id="asp_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Echo:</label><br>
              <p id="echo_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">UroScanner:</label><br>
              <p id="uroscanner_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">UroIRM:</label><br>
              <p id="uroirm_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Densité:</label><br>
              <p id="densite_record" class="col-md-6">
               </p>
             </div>

            </div>
             </div>

            <!--new exploration_fonctionnelle
            <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
              <p id="exploration_record" class="old-visits  col-md-12">
               </p>
             </div>-->
             <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroExploration_record">Exploration fonctionnelle <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>

               <div id="UroExploration_record" class="collapse col-md-12">
                 <div class="col-md-6">
                   <label class="col-md-12">Débimetrie:</label><br>
               <p id="debimetrie_record" class="col-md-6">
                </p>
              </div>
                <div class="col-md-6">
                  <label class="col-md-12">Cystomanometrie:</label><br>
               <p id="cystomano_record" class="col-md-6">
                </p>
              </div>

             </div>
              </div>
           </div>
           <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroDiagnostique_record">Diagnostic <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
             <!---<p id="interrogatoire" class="new-visits  col-md-12">
               <textarea name="interogatoire" class="col-md-12"></textarea>


             </p>--->
             <div id="UroDiagnostique_record" class="collapse col-md-12">
             <div class="col-md-6">
               <label class="col-md-12">TV Superficielle:</label><br>
             <p id="tvs_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">TV infiltrante:</label><br>
             <p id="tvi_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Cancer de prostate:</label><br>
             <p id="cr_prostate_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Cancer du rein:</label><br>
             <p id="cr_rein_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Cancer du testicule:</label><br>
             <p id="cr_testicule_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Lithiase rénale:</label><br>
             <p id="li_renale_record" class="col-md-6">
              </p>
            </div>
             <div class="col-md-6">
               <label class="col-md-12">Lithiase uréterale:</label><br>
             <p id="li_ureterale_record" class="col-md-6">
              </p>
            </div>

           </div>
            </div>

           <div class="row container-fluid">
            <!---<div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
              <p id="diagnostic_record" class="old-visits  col-md-12">
               </p>
            </div>-->
            <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
              <p id="prevention_record" class="old-visits  col-md-12">
               </p>
             </div>
           </div><!--
           <div class="row container-fluid">
             <div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
               <p id="prescription_record" class="old-visits  col-md-12">
                </p>
              </div>
            </div> -->
            <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroTraitement_record">Traitement <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
              <!---<p id="interrogatoire" class="new-visits  col-md-12">
                <textarea name="interogatoire" class="col-md-12"></textarea>


              </p>--->
              <div id="UroTraitement_record" class="collapse col-md-12">
              <div class="col-md-6">
                <label class="col-md-12">MMC:</label><br>
              <p id="mmc_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">BCG:</label><br>
              <p id="bcg_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Prostatectomie:</label><br>
              <p id="prostatectomie_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Analogues LH-RH:</label><br>
              <p id="ana_lh_rh_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Chimiotherapie:</label><br>
              <p id="chimio_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Antiandrogenes:</label><br>
              <p id="antiandro_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Cystoprostatectomie:</label><br>
              <p id="cystopro_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Bricker:</label><br>
              <p id="bricker_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Remplacement de vessie:</label><br>
              <p id="remp_vessie_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Néphrectomie:</label><br>
              <p id="nephrec_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Orchidectomie:</label><br>
              <p id="Orchidec_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">URS rigide:</label><br>
              <p id="urs_rigide_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">URS souple:</label><br>
              <p id="urs_souple_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Lithotripsie:</label><br>
              <p id="lithotripsie_record" class="col-md-6">
               </p>
             </div>

            </div>
             </div>

            <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroAnapath_record">Anapath <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
              <!---<p id="interrogatoire" class="new-visits  col-md-12">
                <textarea name="interogatoire" class="col-md-12"></textarea>


              </p>--->
              <div id="UroAnapath_record" class="collapse col-md-12">
              <div class="col-md-6">
                <label class="col-md-12">TVNIM bas grade:</label><br>
              <p id="tvnimbg_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">TVNIM haut grade:</label><br>
              <p id="tvnimhg_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">TVIM:</label><br>
              <p id="tvim_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Néphrectomie:</label><br>
              <p id="nephrectomie_record" class="col-md-6">
               </p>
             </div>
              <div class="col-md-6">
                <label class="col-md-12">Orchidectomie:</label><br>
              <p id="orchidectomie_record" class="col-md-6">
               </p>
             </div>

            </div>
             </div>

            </div>
        <?php } ?>


        <!-- visit form collaped -->
        <form id="new-visit-container"   class="row collapse">

          <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
            <p id="motif" class="new-visits  col-md-12">
              <textarea name="Visites[motif]" class="col-md-12"></textarea>
            </p>
          </div>
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroInterrogatoire">Interrogatoire <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroInterrogatoire" class="collapse col-md-12">
            <p id="hemodure" class="col-md-6"><label class="col-md-12">Hématurie:</label><br>
                <textarea name="UroInterrogatoire[hemodure]" class="col-md-12"></textarea>
            </p>
            <p id="douleur" class="col-md-6"><label class="col-md-12">Douleur lombaire:</label><br>
              <textarea name="UroInterrogatoire[douleur]" class="col-md-12"></textarea>
            </p>
            <p id="masse" class="col-md-6"><label class="col-md-12">Masse Rénale:</label><br>
                <textarea name="UroInterrogatoire[masse]" class="col-md-12"></textarea>
            </p>
            <p id="grosse" class="col-md-6"><label class="col-md-12">Grosse Bourse:</label><br>
              <textarea name="UroInterrogatoire[grosse]" class="col-md-12"></textarea>
            </p>
            <p id="psa" class="col-md-6"><label class="col-md-12">Elevation du PSA:</label><br>
                <textarea name="UroInterrogatoire[psa]" class="col-md-12"></textarea>
            </p>
            <p id="tuba" class="col-md-6"><label class="col-md-12">Tuba:</label><br>
              <textarea name="UroInterrogatoire[tuba]" class="col-md-12"></textarea>
            </p>
          </div>
           </div>
          <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
            <p id="examen-cli" class="new-visits  col-md-12">
              <textarea name="Visites[examen_clinique]" class="col-md-12"></textarea>
            </p>
          </div><!--
          <div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
            <p id="examen-bio" class="new-visits  col-md-12">
              <textarea name="examen_biologique" class="col-md-12"></textarea>
            </p>
          </div>--->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroBiologique">Examen biologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroBiologique" class="collapse col-md-12">
            <p id="uree" class="col-md-6"><label class="col-md-12">Urée:</label><br>
                <textarea name="UroBiologique[uree]" class="col-md-12"></textarea>
            </p>
            <p id="creat" class="col-md-6"><label class="col-md-12">Créat:</label><br>
              <textarea name="UroBiologique[creat]" class="col-md-12"></textarea>
            </p>
            <p id="mdrd" class="col-md-6"><label class="col-md-12">MDRD:</label><br>
                <textarea name="UroBiologique[mdrd]" class="col-md-12"></textarea>
            </p>
            <p id="biopsa" class="col-md-6"><label class="col-md-12">PSA:</label><br>
              <textarea name="UroBiologique[psa]" class="col-md-12"></textarea>
            </p>
            <p id="bhcg" class="col-md-6"><label class="col-md-12">BHCG:</label><br>
                <textarea name="UroBiologique[bhcg]" class="col-md-12"></textarea>
            </p>
            <p id="afp" class="col-md-6"><label class="col-md-12">αFP:</label><br>
              <textarea name="UroBiologique[afp]" class="col-md-12"></textarea>
            </p>
            <p id="ldh" class="col-md-6"><label class="col-md-12">LDH:</label><br>
                <textarea name="UroBiologique[ldh]" class="col-md-12"></textarea>
            </p>
            <p id="cytologie" class="col-md-6"><label class="col-md-12">Cytologie urinaire:</label><br>
              <textarea name="UroBiologique[cytologie]" class="col-md-12"></textarea>
            </p>
          </div>
        </div><!--
          <div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
            <p id="examen-rad" class="new-visits  col-md-12">
              <textarea name="examen_radiologique" class="col-md-12"></textarea>
            </p>
          </div>-->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroRadiologique">Examen Radiologique <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroRadiologique" class="collapse col-md-12">
            <p id="asp" class="col-md-6"><label class="col-md-12">ASP:</label><br>
                <textarea name="UroRadiologique[asp]" class="col-md-12"></textarea>
            </p>
            <p id="echo" class="col-md-6"><label class="col-md-12">Echo:</label><br>
              <textarea name="UroRadiologique[echo]" class="col-md-12"></textarea>
            </p>
            <p id="uroscanner" class="col-md-6"><label class="col-md-12">Uroscanner:</label><br>
                <textarea name="UroRadiologique[uroscanner]" class="col-md-12"></textarea>
            </p>
            <p id="uroirm" class="col-md-6"><label class="col-md-12">UroIRM:</label><br>
              <textarea name="UroRadiologique[uroirm]" class="col-md-12"></textarea>
            </p>
            <p id="densite" class="col-md-6"><label class="col-md-12">Densité:</label><br>
                <textarea name="UroRadiologique[densite]" class="col-md-12"></textarea>
            </p>

          </div>
           </div>
          <!--new exploration_fonctionnelle
          <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
            <p id="exploration" class="new-visits  col-md-12">
              <textarea name="exploration_fonctionnelle" class="col-md-12"></textarea>
            </p>
           </div>-->
           <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroExploration">Exploration fonctionnelle <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>

             <div id="UroExploration" class="collapse col-md-12">
             <p id="debimetrie" class="col-md-6"><label class="col-md-12">Débimetrie:</label><br>
                 <textarea name="UroExploration[debimetrie]" class="col-md-12"></textarea>
             </p>
             <p id="cystomano" class="col-md-6"><label class="col-md-12">Cystomanometrie:</label><br>
               <textarea name="UroExploration[cystomano]" class="col-md-12"></textarea>
             </p>

           </div>
         </div><!--
          <div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
            <p id="diagnostic" class="new-visits  col-md-12">
              <textarea name="diagnostic" class="col-md-12"></textarea>
            </p>
          </div>-->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroDiagnostique">Diagnostic <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroDiagnostique" class="collapse col-md-12">
            <p id="tvs" class="col-md-6"><label class="col-md-12">TV Superficielle:</label><br>
                <textarea name="UroDiagnostique[tvs]" class="col-md-12"></textarea>
            </p>
            <p id="tvi" class="col-md-6"><label class="col-md-12">TV Superficielle:</label><br>
              <textarea name="UroDiagnostique[tvi]" class="col-md-12"></textarea>
            </p>
            <p id="cr_prostate" class="col-md-6"><label class="col-md-12">Cancer de prostate:</label><br>
                <textarea name="UroDiagnostique[cr_prostate]" class="col-md-12"></textarea>
            </p>
            <p id="cr_rein" class="col-md-6"><label class="col-md-12">Cancer du rein:</label><br>
              <textarea name="UroDiagnostique[cr_rein]" class="col-md-12"></textarea>
            </p>
            <p id="cr_testicule" class="col-md-6"><label class="col-md-12">Cancer du testicule:</label><br>
                <textarea name="UroDiagnostique[cr_testicule]" class="col-md-12"></textarea>
            </p>
            <p id="li_renale" class="col-md-6"><label class="col-md-12">Lithiase rénale:</label><br>
              <textarea name="UroDiagnostique[li_renale]" class="col-md-12"></textarea>
            </p>
            <p id="li_ureterale" class="col-md-6"><label class="col-md-12">Lithiase uréterale:</label><br>
              <textarea name="UroDiagnostique[li_ureterale]" class="col-md-12"></textarea>
            </p>
          </div>
           </div>

          <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
            <p id="prevention" class="new-visits  col-md-12">
              <textarea name="Visites[prevention]" class="col-md-12"></textarea>
            </p>
           </div>
          <!--<div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
            <p id="prescription" class="new-visits  col-md-12">
              <textarea name="prescription" class="col-md-12"></textarea>
            </p>
          </div>-->
          <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroTraitement">Traitement <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
            <!---<p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>


            </p>--->
            <div id="UroTraitement" class="collapse col-md-12">
            <p id="mmc" class="col-md-6"><label class="col-md-12">MMC:</label><br>
                <textarea name="UroTraitement[mmc]" class="col-md-12"></textarea>
            </p>
            <p id="bcg" class="col-md-6"><label class="col-md-12">BCG:</label><br>
              <textarea name="UroTraitement[bcg]" class="col-md-12"></textarea>
            </p>
            <p id="prostatectomie" class="col-md-6"><label class="col-md-12">Prostatectomie:</label><br>
                <textarea name="UroTraitement[prostatectomie]" class="col-md-12"></textarea>
            </p>
            <p id="ana_lh_rh" class="col-md-6"><label class="col-md-12">Analogues LH-RH:</label><br>
              <textarea name="UroTraitement[ana_lh_rh]" class="col-md-12"></textarea>
            </p>
            <p id="chimio" class="col-md-6"><label class="col-md-12">Chimiotherapie:</label><br>
                <textarea name="UroTraitement[chimio]" class="col-md-12"></textarea>
            </p>
            <p id="antiandro" class="col-md-6"><label class="col-md-12">Antiandrogenes:</label><br>
              <textarea name="UroTraitement[antiandro]" class="col-md-12"></textarea>
            </p>
            <p id="cystopro" class="col-md-6"><label class="col-md-12">Cystoprostatectomie:</label><br>
                <textarea name="UroTraitement[cystopro]" class="col-md-12"></textarea>
            </p>
            <p id="bricker" class="col-md-6"><label class="col-md-12">Bricker:</label><br>
              <textarea name="UroTraitement[bricker]" class="col-md-12"></textarea>
            </p>
            <p id="remp_vessie" class="col-md-6"><label class="col-md-12">Remplacement de vessie:</label><br>
              <textarea name="UroTraitement[remp_vessie]" class="col-md-12"></textarea>
            </p>
            <p id="nephrec" class="col-md-6"><label class="col-md-12">Néphrectomie:</label><br>
              <textarea name="UroTraitement[nephrec]" class="col-md-12"></textarea>
            </p>
            <p id="orchidec" class="col-md-6"><label class="col-md-12">Orchidectomie:</label><br>
              <textarea name="UroTraitement[orchidec]" class="col-md-12"></textarea>
            </p>
            <p id="urs_rigide" class="col-md-6"><label class="col-md-12">URS rigide:</label><br>
              <textarea name="UroTraitement[urs_rigide]" class="col-md-12"></textarea>
            </p>
            <p id="urs_souple" class="col-md-6"><label class="col-md-12">URS souple:</label><br>
              <textarea name="UroTraitement[urs_souple]" class="col-md-12"></textarea>
            </p>
            <p id="lithotripsie" class="col-md-6"><label class="col-md-12">Lithotripsie:</label><br>
              <textarea name="UroTraitement[lithotripsie]" class="col-md-12"></textarea>
            </p>
          </div>
          </div>


           <div  class="col-md-12 menu-block"><label class="col-md-12 drop-menu" data-toggle="collapse" data-target="#UroAnapath">Anapath <i class="glyphicon glyphicon-menu-down"></i></label><br><hr>
             <!---<p id="interrogatoire" class="new-visits  col-md-12">
               <textarea name="interogatoire" class="col-md-12"></textarea>


             </p>--->
             <div id="UroAnapath" class="collapse col-md-12">
             <p id="tvnimbg" class="col-md-6"><label class="col-md-12">TVNIM bas grade:</label><br>
                 <textarea name="UroAnapath[tvnimbg]" class="col-md-12"></textarea>
             </p>
             <p id="tvnimhg" class="col-md-6"><label class="col-md-12">TVNIM haut grade:</label><br>
               <textarea name="UroAnapath[tvnimhg]" class="col-md-12"></textarea>
             </p>
             <p id="tvim" class="col-md-6"><label class="col-md-12">TVIM:</label><br>
                 <textarea name="UroAnapath[tvim]" class="col-md-12"></textarea>
             </p>
             <p id="nephrectomie" class="col-md-6"><label class="col-md-12">Néphrectomie:</label><br>
               <textarea name="UroAnapath[nephrectomie]" class="col-md-12"></textarea>
             </p>
             <p id="orchidectomie" class="col-md-6"><label class="col-md-12">Orchidectomie:</label><br>
                 <textarea name="UroAnapath[orchidectomie]" class="col-md-12"></textarea>
             </p>

           </div>
            </div>

           <button id="save-visit" class="row collapse btn btn-success pull-right"   >Enregistrer la visite</button>
         </form>

      <!-- visit form -->

</div>
