<?php

function dd($data)
{
    yii\helpers\VarDumper::dump($data, 10, true);
    Yii::$app->end();
}

function pr($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    Yii::$app->end();
}

function d()
{
    array_map(function ($x){
        echo \yii\helpers\VarDumper::dump($x, 10, true);
    }, func_get_args());
}


 ?>
